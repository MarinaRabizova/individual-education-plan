import { PureComponent } from 'react';
import { connect } from 'react-redux';
import './Subject.css';

import { editSubject, removeSubject, moveSubject } from '../../model/model';

import { 
    editSubjectNameAction,
    editSubjectAuthorAction,
    removeSubjectAction,
    moveSubjectLeftAction,
    moveSubjectRightAction
} from '../../store/actions';


class Subject extends PureComponent {

    moveLeft = async () => {
        const moveData = {
            subjectId: this.props.subjectId,
            subjectArrId: this.props.subjectArrId
        };
        await moveSubject({
            ...moveData,
            destShelfId: moveData.subjectArrId - 1
        });
        this.props.moveSubjectLeftDispatch(moveData);
    }

    moveRight = async () => {
        const moveData = {
            subjectId: this.props.subjectId,
            subjectArrId: this.props.subjectArrId
        };
        await moveSubject({
            ...moveData,
            destShelfId: moveData.subjectArrId + 1
        });
        this.props.moveSubjectRightDispatch(moveData);
    }

    onRemove = async () => {
        const ok = window.confirm('Удалить предмет?');
        if (!ok) {
            return;
        }

        const removeData = {
            subjectId: this.props.subjectId,
            subjectArrId: this.props.subjectArrId
        };
        await removeSubject(removeData);
        this.props.removeSubjectDispatch(removeData);
    }

    onAuthorEdit = async () => {
        let newAuthor = prompt('Введите ФИО нового преподавателя');
        if (!newAuthor || !newAuthor.trim()) {
            alert('Преподавателя с таким ФИО не может быть!');
            return;
        }

        newAuthor = newAuthor.trim();

        const subject = this.props.students[this.props.subjectArrId].subjects[this.props.subjectId];
        const subjectEditData = {
            subjectId: this.props.subjectId,
            subjectArrId: this.props.subjectArrId,
            newAuthor: newAuthor
        };
        await editSubject({
            ...subjectEditData,
            newName: subject.name
        });
        this.props.editSubjectAuthorDispatch(subjectEditData);
    }

    onNameEdit = async () => {
        let newName = prompt('Введите новоe название предмета');
        if (!newName || !newName.trim()) {
            alert('Предмета с таким названием не может быть!');
            return;
        }
        
        newName = newName.trim();

        const subject = this.props.students[this.props.subjectArrId].subjects[this.props.subjectId];
        const subjectEditData = {
            subjectId: this.props.subjectId,
            subjectArrId: this.props.subjectArrId,
            newName: newName,
        };
        await editSubject({
            ...subjectEditData,
            newAuthor: subject.author
        });
        this.props.editSubjectNameDispatch(subjectEditData);
    }

    render() {
        const { subjectId, subjectArrId } = this.props;
        const subject = this.props.students[subjectArrId].subjects[subjectId];

        return (
            <div className="subjectarr-subject">
                <div className="subjectarr-subject-description">
                <div className="subjectarr-subject-name">
                    { subject.name }
                </div>
                <div className="subjectarr-subject-author">
                    { subject.author }
                </div>
                </div>
                
                <div className="subjectarr-subject-controls">
                <div className="subjectarr-subject-controls-row">
                    <div className="subjectarr-subject-controls-icon left-arrow-icon" onClick={this.moveLeft}></div>
                    <div className="subjectarr-subject-controls-icon right-arrow-icon" onClick={this.moveRight}></div>
                </div>
                <div className="subjectarr-subject-controls-row">
                    <div className="subjectarr-subject-controls-icon delete-icon" title="Удалить" onClick={this.onRemove}></div>
                </div>
                <div className="subjectarr-subject-controls-row">
                    <div className="subjectarr-subject-controls-icon editcust-icon" title="Изменить ФИО преподавателя" onClick={this.onAuthorEdit}></div>
                    <div className="subjectarr-subject-controls-icon editdesc-icon" title="Изменить название предмета" onClick={this.onNameEdit}></div>
                </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ students }) => ({ students });

const mapDispatchToProps = dispatch => ({
    editSubjectNameDispatch: ({ subjectId, subjectArrId, newName }) => dispatch(editSubjectNameAction({ subjectId, subjectArrId, newName })),
    editSubjectAuthorDispatch: ({ subjectId, subjectArrId, newAuthor }) => dispatch(editSubjectAuthorAction({ subjectId, subjectArrId, newAuthor })),
    removeSubjectDispatch: ({ subjectId, subjectArrId }) => dispatch(removeSubjectAction({ subjectId, subjectArrId })),
    moveSubjectLeftDispatch: ({ subjectId, subjectArrId }) => dispatch(moveSubjectLeftAction({ subjectId, subjectArrId })),
    moveSubjectRightDispatch: ({ subjectId, subjectArrId }) => dispatch(moveSubjectRightAction({ subjectId, subjectArrId })),
});
  
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Subject);
