import { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';

import Student from '../Student/Student';

import { getStudents, addStudent } from '../../model/model';

import { downloadSubjectsDataAction, addStudentAction } from '../../store/actions';
import './App.css';

class App extends PureComponent {

    state = {
        isInputActive: false,
        inputValue: ''
    };

    async componentDidMount() {
        const students = await getStudents();
        this.props.downloadSubjectsDataDispatch(students);
    }

    inputStudent = () => {
        this.setState({
            isInputActive: true
        });
    }

    onKeyDown = async (event) => {
        if (event.key === 'Escape') {
          this.setState({
            isInputState: false,
            inputValue: ''
          });
        }
    
        if (event.key === 'Enter') {
            const subjectArrName = this.state.inputValue;

            this.setState({
                isInputState: false,
                inputValue: ''
            })
            const subjectArr = { name: subjectArrName, subjects: [] };
            await addStudent(subjectArr);
            this.props.addStudentDispatch(subjectArr);
        }
    }
    
    onInputChange = (event) => {
        this.setState({
            inputValue: event.target.value
        });
    }

    render() {
        const { inputValue, isInputActive } = this.state;

        return (
            <Fragment>
                <header id="main-header">
                    Individual Educaional Plan
                </header>
                <main id="main-container">
                    {this.props.students.map((subjectArr, index) => (
                        <Student key={`subjectarr-${index}`} subjectArrId={index}/>
                    ))}
                    <div className="subjectarr">
                    {isInputActive && <input
                        type="text"
                        id="add-subjectarr-input"
                        placeholder="Имя студента"
                        value={inputValue}
                        onChange={this.onInputChange}
                        onKeyDown={this.onKeyDown}
                    />}
                    {!isInputActive && <header className="subjectarr-name" onClick={this.inputStudent}>
                        Добавить студента
                    </header>}
                    </div>
                </main>
            </Fragment>
        );
    }
}

const mapStateToProps = ({ students }) => ({ students });

const mapDispatchToProps = dispatch => ({
    addStudentDispatch: (subjectArr) => dispatch(addStudentAction(subjectArr)),
    downloadSubjectsDataDispatch: (students) => dispatch(downloadSubjectsDataAction(students)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
