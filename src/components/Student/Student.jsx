import { PureComponent } from 'react';
import { connect } from 'react-redux';
import './Student.css';

import { addSubject } from '../../model/model';

import Subject from '../Subject/Subject';
import { addSubjectAction } from '../../store/actions';


class Student extends PureComponent {

    onSubjectAdd = async () => {
        let subjectName = prompt('Введите название предмета', '');
        if (!subjectName || !subjectName.trim()) {
            alert('Предмета с таким названием не может быть!');
            return;
        }
        subjectName = subjectName.trim();

        let subjectAuthor = prompt('Введите ФИО преподавателя', '').trim();
        if (!subjectAuthor || !subjectAuthor.trim()) {
            alert('Преподавателя с таким ФИО не может быть!');
            return;
        }

        subjectAuthor = subjectAuthor.trim();
        const newSubjectData = {
            subject: {
                name: subjectName,
                author: subjectAuthor
            },
            subjectArrId: this.props.subjectArrId
        };

        await addSubject(newSubjectData);
        this.props.addSubjectDispatch(newSubjectData);
    }

    render() {
        const subjectArrId = this.props.subjectArrId;
        const subjectArr = this.props.students[subjectArrId];

        return (
        <div className="subjectarr">
            <header className="subjectarr-name">
                { subjectArr.name }
            </header>
            <div className="subjectarr-subjects">
                {subjectArr.subjects.map((subject, index) => (
                    <Subject key={`subject-${index}`} subjectId={index} subjectArrId={subjectArrId} />
                ))}
            </div>
            <footer className="subjectarr-add-task" onClick={this.onSubjectAdd}>
                Добавить предмет
            </footer>
        </div>
        );
    }
}

const mapStateToProps = ({ students }) => ({ students });

const mapDispatchToProps = dispatch => ({
    addSubjectDispatch: ({ subject, subjectArrId }) => dispatch(addSubjectAction({ subject, subjectArrId })),
});
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Student);
