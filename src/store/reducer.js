import {
    ADD_STUDENT,
    ADD_SUBJECT,
    EDIT_SUBJECT_NAME,
    EDIT_SUBJECT_AUTHOR,
    REMOVE_SUBJECT,
    DOWNLOAD_SUBJECTS_DATA,
    MOVE_SUBJECT_LEFT,
    MOVE_SUBJECT_RIGHT
} from './actions';

const initialState = {
    students: []
};

export default function reducer(state=initialState, {type, payload}) {
    let subjectToMove = null;

    switch(type) {
    case ADD_STUDENT:
        return {
            ...state,
            students: [
                ...state.students, payload
            ]
        };
    case ADD_SUBJECT:
        return {
            ...state,
            students: state.students.map((subjectArr, index) => (
                index === payload.subjectArrId ? {
                    ...subjectArr,
                    subjects: [...subjectArr.subjects, payload.subject]
                }
                : subjectArr
            ))
        };
    case EDIT_SUBJECT_NAME:
        return {
            ...state,
            students: state.students.map((subjectArr, index) => (
                index === payload.subjectArrId ? {
                    ...subjectArr,
                    subjects: subjectArr.subjects.map((subject, indexSubject) => (
                        indexSubject === payload.subjectId ? {
                            ...subject,
                            name: payload.newName
                        }
                        : subject
                    ))
                }
                : subjectArr
            ))
        };
    case EDIT_SUBJECT_AUTHOR:
        return {
            ...state,
            students: state.students.map((subjectArr, index) => (
                index === payload.subjectArrId ? {
                    ...subjectArr,
                    subjects: subjectArr.subjects.map((subject, indexSubject) => (
                        indexSubject === payload.subjectId ? {
                            ...subject,
                            author: payload.newAuthor
                        }
                        : subject
                    ))
                }
                : subjectArr
            ))
        };
    case REMOVE_SUBJECT:
        return {
            ...state,
            students: state.students.map((subjectArr, index) => (
                index === payload.subjectArrId ? {
                    ...subjectArr,
                    subjects: subjectArr.subjects.filter((subject, subjectIndex) => (subjectIndex !== payload.subjectId))
                }
                : subjectArr
            ))
        };
    case DOWNLOAD_SUBJECTS_DATA:
        return {
            ...state,
            students: payload
        }
    case MOVE_SUBJECT_LEFT:
        subjectToMove = state.students[payload.subjectArrId].subjects[payload.subjectId];

        return {
            ...state,
            students: state.students.map((subjectArr, index) => {
                if (index === payload.subjectArrId) {
                    return {
                        ...subjectArr,
                        subjects: subjectArr.subjects.filter((subject, subjectIndex) => (subjectIndex !== payload.subjectId))
                    };
                }
                if (index === payload.subjectArrId - 1) {
                    return {
                        ...subjectArr,
                        subjects: [...subjectArr.subjects, subjectToMove]
                    };
                }
                return subjectArr;
            })
        };
    case MOVE_SUBJECT_RIGHT:
        subjectToMove = state.students[payload.subjectArrId].subjects[payload.subjectId];

        return {
            ...state,
            students: state.students.map((subjectArr, index) => {
                if (index === payload.subjectArrId) {
                    return {
                        ...subjectArr,
                        subjects: subjectArr.subjects.filter((subject, subjectIndex) => (subjectIndex !== payload.subjectId))
                    };
                }
                if (index === payload.subjectArrId + 1) {
                    return {
                        ...subjectArr,
                        subjects: [...subjectArr.subjects, subjectToMove]
                    };
                }
                return subjectArr;
            })
        };
    default:
        return state;
    }
};
