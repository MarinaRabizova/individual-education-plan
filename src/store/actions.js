const ADD_STUDENT = 'ADD_STUDENT';
const ADD_SUBJECT = 'ADD_SUBJECT';
const EDIT_SUBJECT_NAME = 'EDIT_SUBJECT_NAME';
const EDIT_SUBJECT_AUTHOR = 'EDIT_SUBJECT_AUTHOR';
const REMOVE_SUBJECT = 'REMOVE_SUBJECT';
const DOWNLOAD_SUBJECTS_DATA = 'DOWNLOAD_SUBJECTS_DATA';
const MOVE_SUBJECT_LEFT = 'MOVE_SUBJECT_LEFT';
const MOVE_SUBJECT_RIGHT = 'MOVE_SUBJECT_RIGHT';


const addStudentAction = (subjectArr) => ({
    type: ADD_STUDENT,
    payload: subjectArr
});

const addSubjectAction = ({ subject, subjectArrId }) => ({
    type: ADD_SUBJECT,
    payload: { subject, subjectArrId }
});

const editSubjectNameAction = ({ subjectId, subjectArrId, newName }) => ({
    type: EDIT_SUBJECT_NAME,
    payload: { subjectId, subjectArrId, newName }
});

const editSubjectAuthorAction = ({ subjectId, subjectArrId, newAuthor }) => ({
    type: EDIT_SUBJECT_AUTHOR,
    payload: { subjectId, subjectArrId, newAuthor }
});

const removeSubjectAction = ({ subjectId, subjectArrId }) => ({
    type: REMOVE_SUBJECT,
    payload: { subjectId, subjectArrId }
});

const downloadSubjectsDataAction = (students) => ({
    type: DOWNLOAD_SUBJECTS_DATA,
    payload: students
});

const moveSubjectLeftAction = ({ subjectId, subjectArrId }) => ({
    type: MOVE_SUBJECT_LEFT,
    payload: { subjectId, subjectArrId }
});

const moveSubjectRightAction = ({ subjectId, subjectArrId  }) => ({
    type: MOVE_SUBJECT_RIGHT,
    payload: { subjectId, subjectArrId }
});

export {
    ADD_STUDENT,
    ADD_SUBJECT,
    EDIT_SUBJECT_NAME,
    EDIT_SUBJECT_AUTHOR,
    REMOVE_SUBJECT,
    DOWNLOAD_SUBJECTS_DATA,
    MOVE_SUBJECT_LEFT,
    MOVE_SUBJECT_RIGHT,
    addStudentAction,
    addSubjectAction,
    editSubjectNameAction,
    editSubjectAuthorAction,
    removeSubjectAction,
    downloadSubjectsDataAction,
    moveSubjectLeftAction,
    moveSubjectRightAction
};
