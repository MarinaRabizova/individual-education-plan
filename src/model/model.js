const hostname = 'http://localhost:9999';

const getStudents = async () => {
    const response = await fetch(hostname + '/subjectarr', {method: 'GET'});
    if (response.status !== 200) {
        throw new Error(`getStudents returned ${response.status}`);
    }
    const jsonData = await response.json();
    return jsonData;
};

const addStudent = async (subjectArr) => {
    const response = await fetch(hostname + '/subjectarr', {
        method: 'POST', 
        body: JSON.stringify(subjectArr),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 200) {
        throw new Error(`addStudent returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

const addSubject = async ({ subject, subjectArrId }) => {
    const response = await fetch(hostname + `/subjectarr/${subjectArrId}/subject`, {
        method: 'POST', 
        body: JSON.stringify(subject),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    console.log(response);

    if (response.status !== 200) {
        throw new Error(`addSubject returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

const editSubject = async ({ subjectId, subjectArrId, newName, newAuthor }) => {
    const response = await fetch(hostname + `/subjectarr/${subjectArrId}/subject/${subjectId}`, {
        method: 'PATCH', 
        body: JSON.stringify({ newName: newName, newAuthor: newAuthor }), 
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 200) {
        throw new Error(`editSubjectName returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

const removeSubject = async ({ subjectId, subjectArrId }) => {
    const response = await fetch(hostname + `/subjectarr/${subjectArrId}/subject/${subjectId}`, {
        method: 'DELETE'
    });

    if (response.status !== 200) {
        throw new Error(`removeSubject returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

const moveSubject = async ({ subjectId, subjectArrId, destShelfId }) => {
    const response = await fetch(hostname + `/subjectarr/${subjectArrId}`, {
        method: 'PATCH',
        body: JSON.stringify({ subjectId: subjectId, destShelfId: destShelfId }),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 200) {
        throw new Error(`removeSubject returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

export {
    getStudents,
    addStudent,
    addSubject,
    editSubject,
    removeSubject,
    moveSubject,
};
