const express = require('express');
const app = express();
const { readData, writeData } = require('./utils');

const port = 9999;
const hostname = 'localhost';

let students = [];

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.json());

app.options('/*', (request, response) => {
    response.statusCode = 200;
    response.send('OK');
});

app.get('/subjectarr', async (request, response) => {
    students = await readData();
    response.setHeader('Content-Type', 'application/json');
    response.status(200).json(students);
});

app.post('/subjectarr', async (request, response) => {
    const subjectArr = request.body;
    students.push(subjectArr);
    await writeData(students);
    response.status(200).json({info: 'Subject succefully created!'});
});

app.post('/subjectarr/:subjectArrId/subject', async (request, response) => {
    const subject = request.body;
    const subjectArrId = Number(request.params.subjectArrId);
    students[subjectArrId].subjects.push(subject);
    await writeData(students);
    response.status(200).json({info: 'Subject succefully created!'});
});

app.patch('/subjectarr/:subjectArrId/subject/:subjectId', async (request, response) => {
    const { newName, newAuthor } = request.body;
    const subjectArrId = Number(request.params.subjectArrId);
    const subjectId = Number(request.params.subjectId);

    students[subjectArrId].subjects[subjectId].name = newName;
    students[subjectArrId].subjects[subjectId].author = newAuthor;

    await writeData(students);
    response.status(200).json({info: 'Subject succefully changed!'});
});

app.delete('/subjectarr/:subjectArrId/subject/:subjectId', async (request, response) => {
    const subjectArrId = Number(request.params.subjectArrId);
    const subjectId = Number(request.params.subjectId);

    students[subjectArrId].subjects.splice(subjectId, 1);

    await writeData(students);
    response.status(200).json({info: 'Subject succefully deleted!'});
});

app.patch('/subjectarr/:subjectArrId', async (request, response) => {
    const subjectArrId = Number(request.params.subjectArrId);
    const { subjectId, destShelfId } = request.body;

    const subjectToMove =  students[subjectArrId].subjects.splice(subjectId, 1);
    students[destShelfId].subjects.push(subjectToMove);

    await writeData(students);
    response.status(200).json({info: 'Subject succefully moved!'});
});

app.listen(port, hostname, (error) => {
    if (error) {
        console.error(error);
    }
});
